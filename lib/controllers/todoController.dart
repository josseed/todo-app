import 'package:get/get.dart';
import 'package:todo_app/controllers/authController.dart';
import 'package:todo_app/models/todo.dart';
import 'package:todo_app/services/todoService.dart';

class TodoController extends GetxController {
  Rx<List<Todo>> todoList = Rx<List<Todo>>();

  List<Todo> get todos => todoList.value;

  @override
  void onInit() {
    String uid = Get.find<AuthController>().user.uid;
    todoList.bindStream(TodoService().todoStream(uid)); //stream coming from firebase
    super.onInit();
  }
}