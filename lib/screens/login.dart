import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:todo_app/screens/signup.dart';

class Login extends StatelessWidget {

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login")
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextFormField(decoration: InputDecoration(hintText: "Email")), 
              SizedBox(height: 40,),
              TextFormField(decoration: InputDecoration(hintText: "Password")),
              RaisedButton(
                child: Text("Log In"),
                onPressed: () => print("a")
              ),
              FlatButton(
                child: Text("Sign up"),
                onPressed: () { 
                  Get.to(SignUp());
                }
              )
            ]
          ),
        ),
      ),
    );
  }
}